# ESRF LaTeX Beamer Presentation Template
This introduces the **Grenoble** theme to the Beamer package for presentations' preparation using LaTeX.

## Usage
* Needed LaTeX packages:
 * Beamer
 * TikZ
* Compile using: `pdflatex <TEXFILENAME>.tex`.
 * **Compile twice if using** `\TitPageWide` **style page.**
* All the graphics need to be in the **graphics** directory, or the path to them should be specified.
* Familiarize yourself with the many features Beamer has to offer - there are much more features than seems at first.
* The format of the presentation can be 4x3 (default) or 16x9 (widescreen). See the *.tex* file for details.
* The file *Extra_colors.tex* contains the hex codes for some additional colors that can be used.

## License
This code is distributed under the GNU Public License.
You can redistribute and/or modify it under the terms of
the GNU General Public License as published by the
Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

The rights for the images used in the examples belong to ESRF.
